<?php
class CurltwosController extends Controller {
	public $uses = array('STwoU','STwo','STwoG','STwoL','Question','User');
	
	public function YeniOyunAc(){
        $this->autoRender = false;
        $return = array('hata'=>true);
		$named = $this->request->params['named'];
		$oyun = $named['oyun'];
        // Son Açılan oyun hangisi onu al.
        $sonOyun = $this->STwoG->find('first',array('conditions'=>array('asama != 0'),'order'=>array('id'=>'DESC')));
		if($sonOyun && $sonOyun['STwoG']['id'] == $oyun && $sonOyun['STwoG']['asama'] == 4 && $sonOyun['STwoG']['id'] < 10){
			$sonuc = $this->OyunVerileriKaydet($oyun);
            return $sonuc;
		}else if(empty($sonOyun)){
			$sonuc = $this->OyunVerileriKaydet();
			$this->LinkAta();
			return $sonuc;
		}else{
			$this->STwo->updateAll(array('asama'=>5),array('asama'=>4, 'oyun'=>$oyun));
            $this->STwoG->updateAll(array('asama'=>5),array('asama'=>4, 'id'=>$oyun));
			return false;
		}
    }

    private function OyunVerileriKaydet($sonoyun = 0){
        $this->autoRender = false;
        $this->autoLayout = false;

        $sonOyun = $this->STwoG->find('first',array('conditions'=>array('asama != 0'),'order'=>array('id'=>'DESC')));
        if($sonOyun){
            $acilacakOyunVarMi = $this->STwo->find('all',array('conditions'=>array('oyun'=>$sonOyun['STwoG']['id'])));
            if(empty($acilacakOyunVarMi)){
                return false;
            }
        }

        $this->STwoG->create();
        $this->STwoG->save(array('oyun_tarih'=>date('Y-m-d H:i:s'),'asama'=>0));
        $oyun = $this->STwoG->getLastInsertID();

        // Oyuncuları Al
        $oyuncular = $this->STwoU->find('list',array('fields'=>array('mid')));
        $oyuncuesleri = $this->RandOyuncuArray($oyuncular);
        foreach($oyuncuesleri as $key=>$val){
            // Yeni oyun verilerini kaydet
            $saved = array('gonderen_mid'=>$key,'cevaplayan_mid'=>$val, 'oyun'=>$oyun, 'asama'=>0);
            $this->STwo->create();
            $this->STwo->save($saved);
        }
		
		if($sonoyun != 0){
			$this->STwo->updateAll(array('asama'=>5),array('asama'=>4, 'oyun'=>$sonoyun));
            $this->STwoG->updateAll(array('asama'=>5),array('asama'=>4, 'id'=>$sonoyun));
		}
		$this->STwo->updateAll(array('asama'=>1),array('asama'=>0, 'oyun'=>$oyun));
        $this->STwoG->updateAll(array('asama'=>1),array('asama'=>0, 'id'=>$oyun));
        return true;
    }
}
