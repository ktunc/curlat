<?php
class CurlthreesController extends Controller {
	public $uses = array('SThreeU','SThree','SThreeG','SThreeL','Question','User');
	public $RandOyuncuArr;
    public $OyuncuSayisi = 10;
    public $Round = 10;
	public $OyunArray = array(1,2,3,4,5,6,7,8,9,10);
	public $OyunAsamaArray = array(0,1,2,3,4,5);

	private function FuncUpdateSThree($saved, $where){
	    $durum = false;
        while($durum == false){
            if($this->SThree->find('all',array('conditions'=>$where))){
                $durum = $this->SThree->updateAll($saved, $where);
            }else{
                $durum = true;
            }
        }
    }

    private function FuncUpdateSThreeG($saved, $where){
        $durum = false;
        while($durum == false){
            if($this->SThreeG->find('all',array('conditions'=>$where))){
                $durum = $this->SThreeG->updateAll($saved, $where);
            }else{
                $durum = true;
            }
        }
    }

	public function YeniOyunAc(){
        $this->autoRender = false;
		$named = $this->request->params['named'];
		if(!array_key_exists("oyun",$named) || !in_array($named["oyun"],$this->OyunArray)){
			return false;
		}

		$oyun = $named['oyun'];
		$this->SThree->deleteAll(array('OR'=>array('gonderen_mid IS NULL','cevaplayan_mid IS NULL','oyun IS NULL')),false);
        // Son Açılan oyun hangisi onu al.
		$sonOyun = $this->SThreeG->find('first',array('conditions'=>array('asama != 0'),'order'=>array('id'=>'DESC')));
		
        if($sonOyun && $sonOyun['SThreeG']['id'] != $oyun){
			return false;
		}else if($sonOyun && $sonOyun['SThreeG']['id'] == $oyun && $sonOyun['SThreeG']['asama'] == 4 && $sonOyun['SThreeG']['id'] < 10){
            $sonuc = $this->OyunVerileriKaydet($oyun);
            return $sonuc;
        }else if(empty($sonOyun)){
			$sonuc = $this->OyunVerileriKaydet();
			$this->LinkAta();
			return $sonuc;
		}else{
            $this->FuncUpdateSThree(array('asama'=>5),array('asama'=>4, 'oyun'=>$oyun));
            $this->FuncUpdateSThreeG(array('asama'=>5),array('asama'=>4, 'id'=>$oyun));
			return false;
        }
    }

    private function OyunVerileriKaydet($sonoyun = 0){
        $this->autoRender = false;
        $this->autoLayout = false;
		$gelenoyun = $sonoyun;

        $sonOyun = $this->SThreeG->find('first',array('conditions'=>array('asama != 0'),'order'=>array('id'=>'DESC')));
        if($sonOyun){
            $acilacakOyunVarMi = $this->SThree->find('all',array('conditions'=>array('oyun'=>$sonOyun['SThreeG']['id'])));
            if(empty($acilacakOyunVarMi)){
                return false;
            }
        }

		if(($sonOyun && $sonOyun['SThreeG']['id'] == $gelenoyun && $gelenoyun != 0) || $gelenoyun == 0){
			$gelenoyun++;
			$this->SThreeG->create();
			if(!$this->SThreeG->save(array('id'=>$gelenoyun,'oyun_tarih'=>date('Y-m-d H:i:s'),'asama'=>0))){
				return false;
			}
			$oyun = $this->SThreeG->getLastInsertID();
		}else{
			return false;
		}

        // Oyuncuları Al
        $oyuncular = $this->SThreeU->find('list',array('fields'=>array('mid')));
        $oyuncuesleri = $this->RandOyuncuArray($oyuncular);
        foreach($oyuncuesleri as $key=>$val){
			$keyVarMi = $this->SThree->find('count',array('conditions'=>array('oyun'=>$oyun,'OR'=>array('gonderen_mid'=>$key,'cevaplayan_mid'=>$key))));
			$valVarMi = $this->SThree->find('count',array('conditions'=>array('oyun'=>$oyun,'OR'=>array('gonderen_mid'=>$val,'cevaplayan_mid'=>$val))));
			if($keyVarMi == 0 && $valVarMi == 0){
				// Yeni oyun verilerini kaydet
				$saved = array('gonderen_mid'=>$key,'cevaplayan_mid'=>$val, 'oyun'=>$oyun, 'asama'=>0);
				$this->SThree->create();
				$this->SThree->save($saved);
			}            
        }
		
		if($sonoyun != 0){
			$this->FuncUpdateSThree(array('asama'=>5),array('asama'=>4, 'oyun'=>$sonoyun));
            $this->FuncUpdateSThreeG(array('asama'=>5),array('asama'=>4, 'id'=>$sonoyun));
		}
		
		if($this->SThreeG->findByIdAndAsama($oyun,0)){
			$this->SThreeG->id = $oyun;
			$this->SThreeG->set('asama',1);
			$this->SThreeG->save();
		}
		// $this->FuncUpdateSThreeG(array('asama'=>1),array('asama'=>0, 'id'=>intval($oyun)));
		$this->FuncUpdateSThree(array('asama'=>1),array('asama'=>0, 'oyun'=>$oyun));
        return true;
    }

    private function RandOyuncuArray($oyuncular){
        $this->autoRender = false;
        $this->RandOyuncuArr = array();
        //$randArray = array();
        shuffle($oyuncular);
        $gonderen = array_slice($oyuncular,0,$this->OyuncuSayisi/2);
        $cevaplayan = array_slice($oyuncular,$this->OyuncuSayisi/2,$this->OyuncuSayisi/2);
        foreach($gonderen as $grow){
            $conditions = array(
                "mid NOT IN (SELECT cevaplayan_mid FROM sthree WHERE gonderen_mid = '".$grow."')",
                "mid NOT IN (SELECT cevaplayan_mid FROM sthree WHERE cevaplayan_tarih IS NULL)",
                "mid !='".$grow."'",
                "mid IN ("."'".implode("','",array_values($cevaplayan))."')",
                !empty($this->RandOyuncuArr)?"mid NOT IN ("."'".implode("','",array_values($this->RandOyuncuArr))."')":"1 = 1"
            );
            $BostakiOyuncular = $this->SThreeU->find('list',array('fields'=>array('mid'),'conditions'=>$conditions));
            $bosoyuncular = array_values($BostakiOyuncular);
            $bostaki = $bosoyuncular[rand(0,count($bosoyuncular)-1)];
            unset($cevaplayan[array_search($bostaki,$cevaplayan)]);
            $this->RandOyuncuArr[$grow] = $bostaki;
            if(empty($bosoyuncular)){
                $this->RandOyuncuArray($oyuncular);
            }
        }

        return $this->RandOyuncuArr;
    }
	
	private function LinkAta(){
        $this->autoRender = false;
        $this->autoLayout = false;
        $oyuncular = $this->SThreeU->find('all');
        foreach($oyuncular as $row){
            $diger = $this->SThreeU->find('list',array('fields'=>array('mid'),'conditions'=>array('NOT'=>array('mid'=>$row['SThreeU']['mid']))));
            shuffle($diger);
            if($row['SThreeU']['link'] == 0){
                // Bir directlink ata
                $rand = rand(0,(count($diger)-1));
                // $saved = array('mid'=>$row['SThreeU']['mid'], 'lid'=>$diger[$rand], 'link'=>1);
                $saved = array('mid'=>$row['SThreeU']['mid'], 'lid'=>$diger[$rand], 'link'=>0);
                $this->SThreeL->create();
                $this->SThreeL->save($saved);
            }else{
                // bir directlink ata, bir indirect link ata
                // $rand = rand(0,(count($diger)-1));
                // $saved = array('mid'=>$row['SThreeU']['mid'], 'lid'=>$diger[$rand], 'link'=>1);
                // $this->SThreeL->create();
                // $this->SThreeL->save($saved);
                // unset($diger[$rand]);
				$diger = array_values($diger);
                // bir indirect link ata
                for($i = 0; $i < 8; $i++){
                    $rand = rand(0,(count($diger)-1));
                    $saved = array('mid'=>$row['SThreeU']['mid'], 'lid'=>$diger[$rand], 'link'=>0);
                    $this->SThreeL->create();
                    $this->SThreeL->save($saved);
                    unset($diger[$rand]);
					$diger = array_values($diger);
                }
            }
        }
        return true;
    }
	
	public function osdoldu(){
		$this->autoRender = false;
        $this->autoLayout = false;
		$named = $this->request->params['named'];
		if(!array_key_exists("oyun",$named) || !in_array($named["oyun"],$this->OyunArray)){
            return $this->redirect(
                array('controller' => 'pages', 'action' => 'home')
            );
        }

        if(!array_key_exists("asama",$named) || !in_array($named["asama"],$this->OyunAsamaArray)){
            return $this->redirect(
                array('controller' => 'pages', 'action' => 'home')
            );
        }

		$oyun = $named['oyun'];
		$asama = $named['asama'];
		$oyunData = $this->SThreeG->findById($oyun);

            if($oyunData['SThreeG']['asama'] != $asama || !$oyunData){
                return $this->redirect(
					array('controller' => 'pages', 'action' => 'home')
				);
            }
			$this->SThree->deleteAll(array('OR'=>array('gonderen_mid IS NULL','cevaplayan_mid IS NULL','oyun IS NULL')),false);
            if($asama == 1) // Para gönder
            {
                $gon = $this->SThree->find('all',array('conditions'=>array('gonderen_tutar IS NULL', 'gonderen_tarih IS NULL', 'asama'=>1, 'oyun'=>$oyun)));
                foreach($gon as $cow){
                    $gonderentutar = 0;
                    if($oyun == 1){
                        $gonderentutar = 30;
                    }else{
                        $total = $this->SThree->find('count',array('conditions'=>array('gonderen_mid'=>$cow['SThree']['gonderen_mid'], 'gdurum'=>0)));
                        if($total>0){
                            $gonderentutar = rand(0,100);
                        }
                    }

                    $save = array('gonderen_tutar'=>$gonderentutar,'gonderen_tarih'=>"'".date('Y-m-d H:i:s')."'", 'asama'=>2, 'gdurum'=>0);
                    $this->FuncUpdateSThree($save,array('id'=>$cow['SThree']['id'],'asama'=>1, 'oyun'=>$oyun));
                }
                $this->FuncUpdateSThreeG(array('asama'=>2),array('asama'=>1, 'id'=>$oyun));
            }
            else if($asama == 2) // Cevap Ver
            {
                $cev = $this->SThree->find('all',array('conditions'=>array('cevaplayan_tutar IS NULL', 'cevaplayan_tarih IS NULL', 'asama'=>2, 'oyun'=>$oyun)));
                foreach($cev as $cow){
                    $cevtutar = 0;
                    if($oyun == 1){
                        $cevtutar = 27;
                    }else{
                        $total = $this->SThree->find('count',array('conditions'=>array('cevaplayan_mid'=>$cow['SThree']['cevaplayan_mid'], 'cdurum'=>0)));
                        if($cow['SThree']['gonderen_tutar'] != 0 && $total>0){
                            $cevtutar = rand(0,($cow['SThree']['gonderen_tutar']*3));
                        }
                    }

                    $save = array('cevaplayan_tutar'=>$cevtutar,'cevaplayan_tarih'=>"'".date('Y-m-d H:i:s')."'", 'asama'=>3, 'cdurum'=>0);
                    $this->FuncUpdateSThree($save,array('id'=>$cow['SThree']['id'], 'asama'=>2, 'oyun'=>$oyun));
                }
                $this->FuncUpdateSThreeG(array('asama'=>3),array('asama'=>2, 'id'=>$oyun));
            }else if($asama == 3){
                $this->FuncUpdateSThree(array('asama'=>4),array('asama'=>3, 'oyun'=>$oyun));
                $this->FuncUpdateSThreeG(array('asama'=>4),array('asama'=>3, 'id'=>$oyun));
            }else if($asama == 4){
                $oyunData = $this->SThreeG->findById($oyun);
                if($oyun >= $this->Round && $oyunData['SThreeG']['asama'] == 5){
                }else{
					$this->urloyunac($oyun);
                }
            }
		
		
		return $this->redirect(
            array('controller' => 'pages', 'action' => 'home')
        );
	}
	
	public function urloyunac($oyun=1){
		$this->autoRender = false;
        $this->autoLayout = false;
		
		$url = 'http://'.CURL_URL.'/curlthrees/YeniOyunAc/oyun:'.$oyun;
		$curl = curl_init();                
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl,  CURLOPT_RETURNTRANSFER, false);
		curl_exec($curl);   
		curl_close($curl);  
		
		return $this->redirect(
            array('controller' => 'pages', 'action' => 'home')
        );
	}
}
