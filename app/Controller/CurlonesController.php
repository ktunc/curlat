<?php
class CurlonesController extends Controller {
	public $uses = array('SOneU','SOne','SOneG','Question','User');
	public $RandOyuncuArr;
    public $OyuncuSayisi = 12;
    public $Round = 10;
	
	public function YeniOyunAc(){
        $this->autoRender = false;
        $return = array('hata'=>true);
		$named = $this->request->params['named'];
		$oyun = $named['oyun'];
        // Son Açılan oyun hangisi onu al.
		$this->SOne->deleteAll(array('OR'=>array('gonderen_mid IS NULL','cevaplayan_mid IS NULL')),false);
        $sonOyun = $this->SOneG->find('first',array('conditions'=>array('asama != 0'),'order'=>array('id'=>'DESC')));
		
        if($sonOyun && $sonOyun['SOneG']['id'] != $oyun){
			return false;
		}else if($sonOyun && $sonOyun['SOneG']['id'] == $oyun && $sonOyun['SOneG']['asama'] == 3 && $sonOyun['SOneG']['id'] < 10){
            $sonuc = $this->OyunVerileriKaydet($oyun);
            return $sonuc;
        }else if(empty($sonOyun)){
			$sonuc = $this->OyunVerileriKaydet();
			return $sonuc;
		}else{
            $this->SOne->updateAll(array('asama'=>5),array('asama'=>3, 'oyun'=>$oyun));
            $this->SOneG->updateAll(array('asama'=>5),array('asama'=>3, 'id'=>$oyun));
			return false;
        }
    }

    private function OyunVerileriKaydet($sonoyun = 0){
        $this->autoRender = false;
        $this->autoLayout = false;
		$gelenoyun = $sonoyun;

        $sonOyun = $this->SOneG->find('first',array('conditions'=>array('asama != 0'),'order'=>array('id'=>'DESC')));
        if($sonOyun){
            $acilacakOyunVarMi = $this->SOne->find('all',array('conditions'=>array('oyun'=>$sonOyun['SOneG']['id'])));
            if(empty($acilacakOyunVarMi)){
                return false;
            }
        }

		if(($sonOyun && $sonOyun['SOneG']['id'] == $gelenoyun && $gelenoyun != 0) || $gelenoyun == 0){
			$gelenoyun++;
			$this->SOneG->create();
			if(!$this->SOneG->save(array('id'=>$gelenoyun,'oyun_tarih'=>date('Y-m-d H:i:s'),'asama'=>0))){
				return false;
			}
			$oyun = $this->SOneG->getLastInsertID();
		}else{
			return false;
		}
        

        // Oyuncuları Al
        $oyuncular = $this->SOneU->find('list',array('fields'=>array('mid')));
        $oyuncuesleri = $this->RandOyuncuArray($oyuncular);
        foreach($oyuncuesleri as $key=>$val){
			$keyVarMi = $this->SOne->find('count',array('conditions'=>array('oyun'=>$oyun,'OR'=>array('gonderen_mid'=>$key,'cevaplayan_mid'=>$key))));
			$valVarMi = $this->SOne->find('count',array('conditions'=>array('oyun'=>$oyun,'OR'=>array('gonderen_mid'=>$val,'cevaplayan_mid'=>$val))));
			if($keyVarMi == 0 && $valVarMi == 0){
				// Yeni oyun verilerini kaydet
				$saved = array('gonderen_mid'=>$key,'cevaplayan_mid'=>$val, 'oyun'=>$oyun, 'asama'=>0);
				$this->SOne->create();
				$this->SOne->save($saved);
			}
            
        }
		
        if($sonoyun != 0){
			$this->SOne->updateAll(array('asama'=>5),array('asama'=>3, 'oyun'=>$sonoyun));
            $this->SOneG->updateAll(array('asama'=>5),array('asama'=>3, 'id'=>$sonoyun));
		}
		$this->SOne->updateAll(array('asama'=>1),array('asama'=>0, 'oyun'=>$oyun));
        $this->SOneG->updateAll(array('asama'=>1),array('asama'=>0, 'id'=>$oyun));
        return true;
    }

    private function RandOyuncuArray($oyuncular){
        $this->autoRender = false;
        $this->RandOyuncuArr = array();
        //$randArray = array();
        shuffle($oyuncular);
        $gonderen = array_slice($oyuncular,0,$this->OyuncuSayisi/2);
        $cevaplayan = array_slice($oyuncular,$this->OyuncuSayisi/2,$this->OyuncuSayisi/2);
        foreach($gonderen as $grow){
            $conditions = array(
                "mid NOT IN (SELECT cevaplayan_mid FROM sone WHERE gonderen_mid = '".$grow."')",
                "mid NOT IN (SELECT cevaplayan_mid FROM sone WHERE cevaplayan_tarih IS NULL)",
                "mid !='".$grow."'",
                "mid IN ("."'".implode("','",array_values($cevaplayan))."')",
                !empty($this->RandOyuncuArr)?"mid NOT IN ("."'".implode("','",array_values($this->RandOyuncuArr))."')":"1 = 1"
            );
            $BostakiOyuncular = $this->SOneU->find('list',array('fields'=>array('mid'),'conditions'=>$conditions));
            $bosoyuncular = array_values($BostakiOyuncular);
            $bostaki = $bosoyuncular[rand(0,count($bosoyuncular)-1)];
            unset($cevaplayan[array_search($bostaki,$cevaplayan)]);
            $this->RandOyuncuArr[$grow] = $bostaki;
            if(empty($bosoyuncular)){
                $this->RandOyuncuArray($oyuncular);
            }
        }

        return $this->RandOyuncuArr;
    }
	
	public function osdoldu(){
		$this->autoRender = false;
		$this->autoLayout = false;
		$named = $this->request->params['named'];
		$oyun = $named['oyun'];
		$asama = $named['asama'];
		$oyunData = $this->SOneG->findById($oyun);

		if($oyunData['SOneG']['asama'] != $asama){
			return $this->redirect(
					array('controller' => 'pages', 'action' => 'home')
				);
		}
		$this->SOne->deleteAll(array('OR'=>array('gonderen_mid IS NULL','cevaplayan_mid IS NULL')),false);
		if($asama == 1) // Para gönder
		{
			$gon = $this->SOne->find('all',array('conditions'=>array('gonderen_tutar IS NULL', 'gonderen_tarih IS NULL', 'asama'=>1, 'oyun'=>$oyun)));
			foreach($gon as $cow){
				$total = 0;
				$gonderentutar = 0;
				if($oyun != 1){
					$total = $this->SOne->find('count',array('conditions'=>array('oyun'=>$oyun-1,'OR'=>array(array('gonderen_mid'=>$cow['SOne']['gonderen_mid'], 'gdurum'=>0),array('cevaplayan_mid'=>$cow['SOne']['gonderen_mid'], 'cdurum'=>0)))));
				}
				if($total>0){
					$gonderentutar = rand(0,100);
				}
				$save = array('gonderen_tutar'=>$gonderentutar,'gonderen_tarih'=>"'".date('Y-m-d H:i:s')."'", 'asama'=>2, 'gdurum'=>0);
				$this->SOne->updateAll($save,array('id'=>$cow['SOne']['id'],'asama'=>1, 'oyun'=>$oyun));
			}
			$this->SOneG->updateAll(array('asama'=>2),array('asama'=>1, 'id'=>$oyun));
			$return['hata'] = false;
			$return['sonuc'] = 1;
		}
		else if($asama == 2) // Cevap Ver
		{
			$cev = $this->SOne->find('all',array('conditions'=>array('cevaplayan_tutar IS NULL', 'cevaplayan_tarih IS NULL', 'asama'=>2, 'oyun'=>$oyun)));
			foreach($cev as $cow){
				$total = 0;
				$cevtutar = 0;
				if($oyun != 1){
					$total = $this->SOne->find('count',array('conditions'=>array('oyun'=>$oyun-1,'OR'=>array(array('gonderen_mid'=>$cow['SOne']['cevaplayan_mid'], 'gdurum'=>0),array('cevaplayan_mid'=>$cow['SOne']['cevaplayan_mid'], 'cdurum'=>0)))));
				}

				if($cow['SOne']['gonderen_tutar'] != 0 && $total>0){
					$cevtutar = rand(0,$cow['SOne']['gonderen_tutar']);
				}
				$save = array('cevaplayan_tutar'=>$cevtutar,'cevaplayan_tarih'=>"'".date('Y-m-d H:i:s')."'", 'asama'=>3, 'cdurum'=>0);
				$this->SOne->updateAll($save,array('id'=>$cow['SOne']['id'], 'asama'=>2, 'oyun'=>$oyun));
			}
			$this->SOneG->updateAll(array('asama'=>3),array('asama'=>2, 'id'=>$oyun));
		}else if($asama == 3){
			$oyunData = $this->SOneG->findById($oyun);
			if($oyun >= $this->Round && $oyunData['SOneG']['asama'] == 5){
			}else{
				$this->urloyunac($oyun);
			}
		}
		
		return $this->redirect(
			array('controller' => 'pages', 'action' => 'home')
		);
	}
	
	public function urloyunac($oyun=1){
		$this->autoRender = false;
        $this->autoLayout = false;
		
		$url = 'http://b.kaantunc.com/curlones/YeniOyunAc/oyun:'.$oyun;
		$curl = curl_init();                
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl,  CURLOPT_RETURNTRANSFER, false);
		curl_exec($curl);   
		curl_close($curl);  
		
		return $this->redirect(
            array('controller' => 'pages', 'action' => 'home')
        );
	}
}
