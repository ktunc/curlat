<?php
class CurltwosController extends Controller {
	public $uses = array('STwoU','STwo','STwoG','STwoL','Question','User');
	public $RandOyuncuArr;
    public $OyuncuSayisi = 10;
    public $Round = 10;
    public $OyunArray = array(1,2,3,4,5,6,7,8,9,10);
    public $OyunAsamaArray = array(0,1,2,3,4,5);

    private function FuncUpdateSTwo($saved, $where){
        $durum = false;
        while($durum == false){
            if($this->SThree->find('all',array('conditions'=>$where))){
                $durum = $this->STwo->updateAll($saved, $where);
            }else{
                $durum = true;
            }
        }
    }

    private function FuncUpdateSTwoG($saved, $where){
        $durum = false;
        while($durum == false){
            if($this->SThreeG->find('all',array('conditions'=>$where))){
                $durum = $this->STwoG->updateAll($saved, $where);
            }else{
                $durum = true;
            }
        }
    }
	
	public function YeniOyunAc(){
        $this->autoRender = false;
		$named = $this->request->params['named'];
        if(!array_key_exists("oyun",$named) || !in_array($named["oyun"],$this->OyunArray)){
            return false;
        }

		$oyun = $named['oyun'];
		$this->STwo->deleteAll(array('OR'=>array('gonderen_mid IS NULL','cevaplayan_mid IS NULL','oyun IS NULL')),false);
        // Son Açılan oyun hangisi onu al.
        $sonOyun = $this->STwoG->find('first',array('conditions'=>array('asama != 0'),'order'=>array('oyun'=>'DESC')));
		
		if($sonOyun && $sonOyun['STwoG']['oyun'] != $oyun){
			return false;
		}else if($sonOyun && $sonOyun['STwoG']['oyun'] == $oyun && $sonOyun['STwoG']['asama'] == 4 && $sonOyun['STwoG']['oyun'] < 10){
			$sonuc = $this->OyunVerileriKaydet($oyun);
            return $sonuc;
		}else if(empty($sonOyun)){
			$sonuc = $this->OyunVerileriKaydet();
			$this->LinkAta();
			return $sonuc;
		}else{
			$this->FuncUpdateSTwo(array('asama'=>5),array('asama'=>4, 'oyun'=>$oyun));
            $this->FuncUpdateSTwoG(array('asama'=>5),array('asama'=>4, 'oyun'=>$oyun));
			return false;
		}
    }

    private function OyunVerileriKaydet($sonoyun = 0){
        $this->autoRender = false;
        $this->autoLayout = false;
		$gelenoyun = $sonoyun;

        $sonOyun = $this->STwoG->find('first',array('conditions'=>array('asama != 0'),'order'=>array('oyun'=>'DESC')));
        if($sonOyun){
            $acilacakOyunVarMi = $this->STwo->find('all',array('conditions'=>array('oyun'=>$sonOyun['STwoG']['oyun'])));
            if(empty($acilacakOyunVarMi)){
                return false;
            }
        }

		if(($sonOyun && $sonOyun['STwoG']['oyun'] == $gelenoyun && $gelenoyun != 0) || $gelenoyun == 0){
			$gelenoyun++;
			$this->STwoG->create();
			if(!$this->STwoG->save(array('oyun'=>$gelenoyun,'oyun_tarih'=>date('Y-m-d H:i:s'),'asama'=>0))){
				return false;
			}
			$oyun = $gelenoyun;
		}else{
			return false;
		}

        // Oyuncuları Al
        $oyuncular = $this->STwoU->find('list',array('fields'=>array('mid')));
        $oyuncuesleri = $this->RandOyuncuArray($oyuncular);
        foreach($oyuncuesleri as $key=>$val){
			$keyVarMi = $this->STwo->find('count',array('conditions'=>array('oyun'=>$oyun,'OR'=>array('gonderen_mid'=>$key,'cevaplayan_mid'=>$key))));
			$valVarMi = $this->STwo->find('count',array('conditions'=>array('oyun'=>$oyun,'OR'=>array('gonderen_mid'=>$val,'cevaplayan_mid'=>$val))));
			if($keyVarMi == 0 && $valVarMi == 0){
				// Yeni oyun verilerini kaydet
				$saved = array('gonderen_mid'=>$key,'cevaplayan_mid'=>$val, 'oyun'=>$oyun, 'asama'=>0);
				$this->STwo->create();
				$this->STwo->save($saved);
			}
        }
		
		if($sonoyun != 0){
			$this->FuncUpdateSTwo(array('asama'=>5),array('asama'=>4, 'oyun'=>$sonoyun));
            $this->FuncUpdateSTwoG(array('asama'=>5),array('asama'=>4, 'oyun'=>$sonoyun));
		}
		
		if($this->STwoG->findByOyunAndAsama($oyun,0)){
		    $stwogame = $this->STwoG->find('first',array('conditions'=>array('oyun'=>$oyun)));
			$this->STwoG->id = $stwogame['STwoG']['id'];
			$this->STwoG->set('asama',1);
			$this->STwoG->save();
		}
		//$this->FuncUpdateSTwo(array('asama'=>1),array('asama'=>0, 'oyun'=>$oyun));
        $this->FuncUpdateSTwo(array('asama'=>1),array('asama'=>0, 'oyun'=>$oyun));
        return true;
    }
	
	private function RandOyuncuArray($oyuncular){
        $this->autoRender = false;
        $this->RandOyuncuArr = array();
        //$randArray = array();
        shuffle($oyuncular);
        $gonderen = array_slice($oyuncular,0,$this->OyuncuSayisi/2);
        $cevaplayan = array_slice($oyuncular,$this->OyuncuSayisi/2,$this->OyuncuSayisi/2);
        foreach($gonderen as $grow){
            $conditions = array(
                "mid NOT IN (SELECT cevaplayan_mid FROM stwo WHERE gonderen_mid = '".$grow."')",
                "mid NOT IN (SELECT cevaplayan_mid FROM stwo WHERE cevaplayan_tarih IS NULL)",
                "mid !='".$grow."'",
                "mid IN ("."'".implode("','",array_values($cevaplayan))."')",
                !empty($this->RandOyuncuArr)?"mid NOT IN ("."'".implode("','",array_values($this->RandOyuncuArr))."')":"1 = 1"
            );
            $BostakiOyuncular = $this->STwoU->find('list',array('fields'=>array('mid'),'conditions'=>$conditions));
            $bosoyuncular = array_values($BostakiOyuncular);
            $bostaki = $bosoyuncular[rand(0,count($bosoyuncular)-1)];
            unset($cevaplayan[array_search($bostaki,$cevaplayan)]);
            $this->RandOyuncuArr[$grow] = $bostaki;
            if(empty($bosoyuncular)){
                $this->RandOyuncuArray($oyuncular);
            }
        }

        return $this->RandOyuncuArr;
    }
	
	private function LinkAta(){
        $this->autoRender = false;
        $this->autoLayout = false;
        $oyuncular = $this->STwoU->find('all');
        foreach($oyuncular as $row){
            $diger = $this->STwoU->find('list',array('fields'=>array('mid'),'conditions'=>array('NOT'=>array('mid'=>$row['STowU']['mid']))));
            shuffle($diger);
            if($row['STwoU']['link'] == 0){
                // Bir directlink ata
                $rand = rand(0,(count($diger)-1));
                $saved = array('mid'=>$row['STwoU']['mid'], 'lid'=>$diger[$rand], 'link'=>0);
                $this->STwoL->create();
                $this->STwoL->save($saved);
            }else{
                // bir directlink ata, bir indirect link ata
                $rand = rand(0,(count($diger)-1));
                $saved = array('mid'=>$row['STwoU']['mid'], 'lid'=>$diger[$rand], 'link'=>0);
                $this->STwoL->create();
                $this->STwoL->save($saved);
                unset($diger[$rand]);
				$diger = array_values($diger);
                // bir indirect link ata
                $rand = rand(0,(count($diger)-1));
                $saved = array('mid'=>$row['STwoU']['mid'], 'lid'=>$diger[$rand], 'link'=>0);
                $this->STwoL->create();
                $this->STwoL->save($saved);
            }
        }
        return true;
    }
	
	public function osdoldu(){
		$this->autoRender = false;
        $this->autoLayout = false;
		$named = $this->request->params['named'];
        if(!array_key_exists("oyun",$named) || !in_array($named["oyun"],$this->OyunArray)){
            return $this->redirect(
                array('controller' => 'pages', 'action' => 'home')
            );
        }

        if(!array_key_exists("asama",$named) || !in_array($named["asama"],$this->OyunAsamaArray)){
            return $this->redirect(
                array('controller' => 'pages', 'action' => 'home')
            );
        }

		$oyun = $named['oyun'];
		$asama = $named['asama'];
		$oyunData = $this->STwoG->findByOyun($oyun);

            if($oyunData['STwoG']['asama'] != $asama || !$oyunData){
                return $this->redirect(
					array('controller' => 'pages', 'action' => 'home')
				);
            }
			$this->STwo->deleteAll(array('OR'=>array('gonderen_mid IS NULL','cevaplayan_mid IS NULL','oyun IS NULL')),false);
            if($asama == 1) // Para gönder
            {
                $gon = $this->STwo->find('all',array('conditions'=>array('gonderen_tutar IS NULL', 'gonderen_tarih IS NULL', 'asama'=>1, 'oyun'=>$oyun)));
                foreach($gon as $cow){
                    $gonderentutar = 0;
					if($oyun == 1){
                        $gonderentutar = 30;
                    }else{
                        $total = $this->STwo->find('count',array('conditions'=>array('gonderen_mid'=>$cow['STwo']['gonderen_mid'], 'gdurum'=>0)));
                        if($total>0){
                            $gonderentutar = rand(0,100);
                        }
                    }

                    $save = array('gonderen_tutar'=>$gonderentutar,'gonderen_tarih'=>"'".date('Y-m-d H:i:s')."'", 'asama'=>2, 'gdurum'=>0);
                    $this->FuncUpdateSTwo($save,array('id'=>$cow['STwo']['id'],'asama'=>1, 'oyun'=>$oyun));
                }
                $this->FuncUpdateSTwoG(array('asama'=>2),array('asama'=>1, 'oyun'=>$oyun));
            }
            else if($asama == 2) // Cevap Ver
            {
                $cev = $this->STwo->find('all',array('conditions'=>array('cevaplayan_tutar IS NULL', 'cevaplayan_tarih IS NULL', 'asama'=>2, 'oyun'=>$oyun)));
                foreach($cev as $cow){
                    $cevtutar = 0;
                    if($oyun == 1){
                        $cevtutar = 27;
                    }else{
                        $total = $this->STwo->find('count',array('conditions'=>array('cevaplayan_mid'=>$cow['STwo']['cevaplayan_mid'], 'cdurum'=>0)));
                        if($cow['STwo']['gonderen_tutar'] != 0 && $total>0){
                            $cevtutar = rand(0,($cow['STwo']['gonderen_tutar']*3));
                        }
                    }

                    $save = array('cevaplayan_tutar'=>$cevtutar,'cevaplayan_tarih'=>"'".date('Y-m-d H:i:s')."'", 'asama'=>3, 'cdurum'=>0);
                    $this->FuncUpdateSTwo($save,array('id'=>$cow['STwo']['id'], 'asama'=>2, 'oyun'=>$oyun));
                }
                $this->FuncUpdateSTwoG(array('asama'=>3),array('asama'=>2, 'oyun'=>$oyun));
            }else if($asama == 3){
                $this->FuncUpdateSTwo(array('asama'=>4),array('asama'=>3, 'oyun'=>$oyun));
                $this->FuncUpdateSTwoG(array('asama'=>4),array('asama'=>3, 'oyun'=>$oyun));
            }else if($asama == 4){
                $oyunData = $this->STwoG->findByOyun($oyun);
                if($oyun >= $this->Round && $oyunData['STwoG']['asama'] == 5){
                }else{
					$this->urloyunac($oyun);
                }
            }
		
		
		return $this->redirect(
            array('controller' => 'pages', 'action' => 'home')
        );
	}
	
	public function urloyunac($oyun=1){
		$this->autoRender = false;
        $this->autoLayout = false;
		
		$url = 'http://'.CURL_URL.'/curltwos/YeniOyunAc/oyun:'.$oyun;
		$curl = curl_init();                
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl,  CURLOPT_RETURNTRANSFER, false);
		curl_exec($curl);   
		curl_close($curl);  
		
		return $this->redirect(
            array('controller' => 'pages', 'action' => 'home')
        );
	}
}
